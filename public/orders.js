let maxQuantity = 5;
let createProducts = 1;
let totalQuantityProduct = 0;
let idCustomer = null;
let array_products = [];
let orderTotalProducts = 0;
let toDate = '';
let fromDate = '';
let valueToDate = '';
let valueFromDate = '';

alertify.defaults.theme.ok = "btn btn-success";
alertify.defaults.theme.cancel = "btn btn-danger";

function isNumber(evt)
{
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true; 
}

function changeCustomer() {
    $('#orderProduct').typeahead('val','');
    products.clear();
    products.remote.url = 'index.php/getListProduct/'+ document.getElementById('idOrderCustomer').value +'/%QUERY';
    products.initialize(true);
}

var customer = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: 'index.php/getListCustomer/%QUERY',
        wildcard: '%QUERY',
        filter: function(data) {
            var newArray = [];
                data.forEach(function(el){
                    newArray.push(el);
                });
            return newArray;
        }
    }
});

var products = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: 'index.php/getListProduct/0/%QUERY',
        wildcard: '%QUERY'
    }
});

/*Autocomplete customer filters */
$('#customerFilter').typeahead({
    hint: true,
    highlight: true,
    minLength: 3,
    empty: 'Sin resultados',
    async: true
}, {
    display: 'name',
    value: 'customer_id',
    name: 'customer',
    source: customer,
    limit: 20,
    templates: {
        empty: function(context) {
            $(".tt-dataset").text('Sin resultados');
            document.getElementById('idCustomerFilter').value = "";
        }
    }
}).bind("typeahead:selected", function(obj, datum, name) {
    document.getElementById('idCustomerFilter').value = datum.customer_id;
    changeUrl();
});

function reloadInitDataTable(){
    var $table = $('#tableOrders');
    $table.bootstrapTable('refresh', {
        url: 'index.php/listOrders'
    });

    $('#customerFilter').typeahead('val', '');
    document.getElementById('idCustomerFilter').value = '';
    document.getElementById('dateIniFilter').value = '';
    document.getElementById('dateEndFilter').value = '';

}

function clearSearch(){

    var url= "";
    if(toDate !== '' && fromDate === ''){
        url = 'index.php/listOrders?toDate='+valueToDate;
    }

    if(toDate === '' && fromDate !== ''){
        url = 'index.php/listOrders?fromDate='+valueFromDate;
    }

    if(toDate !== '' && fromDate !== ''){
        url = 'index.php/listOrders?fromDate='+valueFromDate+"&toDate="+valueToDate;
    }
    $('#customerFilter').typeahead('val', '');
    document.getElementById('idCustomerFilter').value = '';

    var $table = $('#tableOrders');
    $table.bootstrapTable('refresh', {
        url: url
    });
}

function changeUrl(){
    var url = 'index.php/listOrders?idCustomer='+ document.getElementById('idCustomerFilter').value;

    if(toDate !== '' && fromDate === ''){
        url += '&toDate='+valueToDate;
    }

    if(toDate === '' && fromDate !== ''){
        url += '&fromDate='+valueFromDate;
    }

    if(toDate !== '' && fromDate !== ''){
        url += '&fromDate='+valueFromDate+"&toDate="+valueToDate;
    }

    var $table = $('#tableOrders');
    $table.bootstrapTable('refresh', {
        url: url
    });
}

$('input[type="date"]').change(function(){

    if(this.id === 'dateIniFilter'){
        valueToDate = this.value;
        toDate  = new Date(this.value);
    }

    if(this.id === 'dateEndFilter'){
        valueFromDate = this.value;
        fromDate  = new Date(this.value);
    }

    if(fromDate !== ''){
        if(toDate > fromDate){
            alert("Date To cant higher From");

            $("#"+this.id).val('');

            if(this.id === 'dateIniFilter'){
                toDate  = "";
            }
        
            if(this.id === 'dateEndFilter'){
                fromDate  = "";
            }       

            return false;
        }
    }

    var url= "";
    var idCustomer = document.getElementById('idCustomerFilter').value;
    if(toDate !== '' && fromDate === ''){
        url = 'index.php/listOrders?toDate='+valueToDate;
    }

    if(toDate === '' && fromDate !== ''){
        url = 'index.php/listOrders?fromDate='+valueFromDate;
    }

    if(toDate !== '' && fromDate !== ''){
        url = 'index.php/listOrders?fromDate='+valueFromDate+"&toDate="+valueToDate;
    }

    if(idCustomer !== ''){

        if(url === ''){
            url = 'index.php/listOrders?idCustomer='+idCustomer;
        }else{
            url += '&idCustomer='+idCustomer;
        }
    }

    var $table = $('#tableOrders');
    $table.bootstrapTable('refresh', {
        url: url
    }); 
});



/*Autocomplete customer */
$('#orderCustomer').typeahead({
    hint: true,
    highlight: true,
    minLength: 3,
    empty: 'Sin resultados',
    async: true
}, {
    display: 'name',
    value: 'customer_id',
    name: 'customer',
    source: customer,
    limit: 20,
    templates: {
        //suggestion: function(el){return '<b>'+el.name.toUpperCase()+'</b>' },
        empty: function(context) {
            $(".tt-dataset").text('Sin resultados');
            document.getElementById('idOrderCustomer').value = "";
            idCustomer = null;
        }
    }
}).bind("typeahead:selected", function(obj, datum, name) {
    validateCustomer(datum.customer_id);
});


/*Autocomplete products */
$('#orderProduct').typeahead({
    hint: true,
    highlight: true,
    minLength: 1,
    empty: 'Sin resultados',
    async: true
}, {
    display: 'name',
    value: 'product_id',
    name: 'products',
    source: products,
    limit:10,
    templates: {
        suggestion: function(el){return '<b>'+el.name.toUpperCase()+'</b>' },
        empty: function(context) {
            $(".tt-dataset").text('Sin resultados');
        }
    }
}).bind("typeahead:selected", function(obj, data, name) {
    alertify.confirm('Confirm', 'Confirm add product ?', 
    function(){ 
        if(validateQuantityAdd()){
            if(validateExists(data.product_id)){
                createDiv(data);
                $('#orderProduct').typeahead('val', '');
                alertify.success('Product added');
            }
        }
    }, function(){ alertify.error('Cancel')});
    
});

function validateExists(product_id){
    if(array_products.includes(product_id)){
        alertify.alert('Warning','You cant add product, product existing in list');
        $('#orderProduct').typeahead('val', '');
        return false;
    }else{
        array_products.push(product_id);
        return true;
    }
}

function createDiv(data){
    var labels = [];
    var html = "";

    /** Seteo default  */
    labels['name'] = '';
    labels['q'] = '';
    labels['price'] = '';
    labels['total'] = '';
    labels['delete'] = '';
    /* Valido los label a incluir */
    if(createProducts === 1){
        labels['name'] = '<label for="inputProduct">Product Name</label>';
        labels['q'] = '<label for="orderQuantity">Quantity</label>';
        labels['price'] = '<label for="inlineFormInputGroup">Price</label>';
        labels['total'] = '<label for="inputTotal">Total</label>';
        labels['delete'] = '<label for="deleteProduct">Delete</label>';
    }

    var priceApi = parseInt(data.price);
    var price = new Intl.NumberFormat().format(priceApi);
    html = '<div id="data'+createProducts+'">'+
                    '<div class="form-row">'+
                        '<div class="form-group col-md-4">'+
                        labels['name']+
                        '<input type="text" required readonly class="form-control" id="inputProduct" placeholder="Product" value="'+data.product_description+'">'+
                        '<input type="hidden" id="idProductCustomer'+createProducts+'" name="idProductCustomer'+createProducts+'" value="'+data.product_id+'">'+
                        '</div>'+
                        '<div class="form-group col-md-2">'+
                            labels['q'] +
                            '<input class="form-control form-quantity" type="number" required min="1" max="5" maxlength="1" onchange="return validateQuantityNow(event, this)" onkeyup="return validateQuantityNow(event, this)" onkeypress="  return isNumber(event)" id="orderQuantity'+createProducts+'" name="orderQuantity'+createProducts+'" placeholder="Quantity" value="1">'+
                        '</div>'+
                        '<div class="form-group col-md-3">'+
                            labels['price']+
                            '<div class="input-group mb-2">'+
                                '<div class="input-group-prepend">'+
                                    '<div class="input-group-text">$</div>'+
                                '</div>'+
                                '<input type="number" required value="'+price+'" readonly class="form-control" id="orderPrice'+createProducts+'">'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group col-md-2">'+
                            labels['total'] +
                            '<div class="input-group mb-2">'+
                                '<div class="input-group-prepend">'+
                                    '<div class="input-group-text">$</div>'+
                                '</div>'+
                                '<input type="number" required readonly value="'+price+'" class="form-control" id="inputTotal'+createProducts+'">'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group col-md-1">'+
                            labels['delete'] +
                            '<button type="button" onclick="deleteProductAction('+createProducts+')" class="form-control btn btn-danger" id="deleteProduct">'+
                                '<i class="material-icons">delete</i>'+
                            '</button>'+
                        '</div>'+
                    '</div>'+
                '</div>';
        createProducts++;
        totalQuantityProduct++;
        document.getElementById('orderTotal').innerHTML = new Intl.NumberFormat().format(parseInt(priceApi) + parseInt(orderTotalProducts));
        $("#products").append(html);
}

function validateCustomer(newIdCustomer){

    if(idCustomer === null){
        idCustomer = newIdCustomer;
        document.getElementById('idOrderCustomer').value = newIdCustomer;
    }

    if(idCustomer !== newIdCustomer){
        alertify.confirm('Confirm', 'Want change customer?', 
        function(){ 
            deleteInfoDiv();
            idCustomer = newIdCustomer;
            array_products = [];
            createProducts = 1;
            totalQuantityProduct = 0;
            document.getElementById('idOrderCustomer').value = newIdCustomer;
            alertify.success('Changed Customer'); 
        }, function(){ alertify.error('Cancel')});
    }

}

function deleteInfoDiv(){
    /** Reinicio valores */
    totalQuantityProduct = 0;

    /** Limpio div */
    document.getElementById('products').innerHTML = '';
    $('#orderProduct').typeahead('val', '');
}

function deleteProductAction(id){
    /** Borro div */
    var el = document.getElementById('data'+id);
    var elements = el.getElementsByTagName('input');
    var i = array_products.indexOf( document.getElementById('idProductCustomer'+id).value );
    array_products.splice( i, 1 );
    el.parentNode.removeChild(el);

    /** Recalcular todas de elementos */
    calcTotalProducts();
    $('#orderProduct').typeahead('val', '');
}

function calcTotalProducts(){

    var sumTemp = 0;
    var el = document.getElementsByClassName("form-quantity");
    if(el){
        el.forEach(function(element){
            sumTemp = parseInt(sumTemp) + parseInt(element.value);
        });
    }
    totalQuantityProduct = sumTemp;
}

function validateQuantityNow(event, el){
    calcTotalProducts();

    var totalTmp = parseInt(totalQuantityProduct);
    if(totalTmp > maxQuantity){
        alertify.alert('Warning',"The limit is 5 in product's total quantity");
        el.value = 1;
        calcTotalsAllProducts();
        return false;
    }
    calcTotalsAllProducts();
}

function validateQuantityAdd(){
    calcTotalProducts();
    var totalTmp = parseInt(totalQuantityProduct) + 1;
    if(totalTmp > maxQuantity){
        alertify.alert('Warning',"The limit is 5 in product's total quantity, you can't add more elements");
        calcTotalsAllProducts();
        $('#orderProduct').typeahead('val', '');
        return false;
    }
    calcTotalsAllProducts();
    return true;
}


function calcTotalsAllProducts(){

    var orderTotal = 0;
    for(var i = 0; i <= createProducts; i++){
        var q = document.getElementById('orderQuantity'+i);
        if(q){
            var price = document.getElementById('orderPrice'+i).value;
            var newPrice = price.replace('.', "");
            var total = parseInt(q.value) * parseInt(newPrice);
            orderTotal = parseInt(total) + parseInt(orderTotal);
            document.getElementById('inputTotal'+i).value = new Intl.NumberFormat().format(total);
        }
    }
    orderTotalProducts = orderTotal;
    document.getElementById('orderTotal').innerHTML = new Intl.NumberFormat().format(orderTotalProducts);
}

document.getElementById('form-order').onsubmit = function (e) {

    e.preventDefault();

    var eleDiv = document.getElementById('products').getElementsByTagName('input');
    if(eleDiv.length === 0){
        alertify.alert('Warning','Please, add one product for save order');
        return false;
    }
    
    var sumTemp = 0;
    var elemQuantity = document.getElementsByClassName("form-quantity");
    if(elemQuantity){
        elemQuantity.forEach(function(element){
            if(parseInt(element.value) < 0){
                alertify.alert('Warning','Please, quantity minium is 1 ');
                return false;
            }
            sumTemp = parseInt(sumTemp) + parseInt(element.value);
        });
    }

    if(sumTemp > maxQuantity){
        alertify.alert('Warning','Please, quantity maximum is : ' + maxQuantity);
        return false;
    }

    document.getElementById('productsCreate').value = createProducts;

    var inputs = document.querySelector("form").elements;
    var values = {};

    for (var i = 0; i < inputs.length; i++) {
        values[inputs[i].name] = inputs[i].value;
    }

    var msjConfirm = 'Confirm data orden <b>' +
            '<br> Data: ' +
            '<br><b> Nombre: </b>' + document.getElementById('orderCustomer').value +
            '<br><b> Total: </b>' + document.getElementById('orderTotal').innerHTML;

    alertify.confirm('Confirm',
            msjConfirm,
            function () {
                var data = btoa(JSON.stringify(values));
                var http = new XMLHttpRequest();
                var params = 'data=' + data;
                var url = "index.php/createOrder/";
                http.open("POST", url, true);
                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                http.setRequestHeader('HTTP_X_REQUESTED_WITH', 'XMLHttpRequest');
                http.onreadystatechange = function () {
                    if (http.readyState === 4 && http.status === 200) {
                        
                        var data = this.responseText;
                        alertify.alert('Mensaje', data);
                        deleteInfoDiv();
                        createProducts = 1;
                        totalQuantityProduct = 0;
                        idCustomer = null;
                        array_products = [];
                        orderTotalProducts = 0;
                        document.getElementById('orderAddress').value = '';
                        document.getElementById('orderTotal').innerHTML = '';
                        $('#orderProduct').typeahead('val', '');
                        $('#orderCustomer').typeahead('val', '');
                        
                    } else {
                        alertify.alert("Error", "Favor comunicarse con el area de sistemas");
                    }
                }
                http.send(params);

            },
            function () {
                alertify.error('No se creo orden');
            }
    );
}