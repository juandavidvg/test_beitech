<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

# List customers
$route['getListCustomer/(:any)'] = 'Orders/getDataCustomers/$1';
$route['getListProduct/(:num)/(:any)'] = 'Orders/getDataProductsById/$1/$2';
$route['createOrder']['POST'] = 'Orders/setDataOrder';
$route['listOrders']['GET'] = 'Orders/getDataTable';
