<?php
/**
 * Description of Orders_model
 *
 * @author jdvasquez
 */
class Orders_model extends CI_Model {

    /**
     * Method for get customer list  filter by name
     * @param String $search Nombre de busqueda 
     * @return Array
     */
    public function getCustomerByName($search) {
        $sql = "SELECT customer_id, UPPER(name) AS name, email 
                FROM test.customer
                WHERE name LIKE '%$search%'
                ORDER BY name;";
        return $this->db->query($sql)->result();
    }

    /**
     * Method for get product list filter customer
     * @param Int $customer_id Customers ID
     * @return Array
     */
    public function getProductsByCustomer($customer_id, $search){
        $sql = "SELECT p.* 
                FROM test.product p
                INNER JOIN customer_product cp ON p.product_id = cp.product_id
                WHERE cp.customer_id =  $customer_id AND name LIKE '%$search%';";
        return $this->db->query($sql)->result();
    }

    /**
     * Method for get product list filter ID
     * @param Int $productId Product ID
     * @return Array
     */
    public function getDataProduct($productId){
        $sql = "SELECT * 
                FROM test.product p
                WHERE p.product_id = $productId;";
        return $this->db->query($sql)->result();
    }

    /**
     * Method create head order
     * @param Array $data Data insert
     */
    public function createOrder($data){
        $this->db->insert('order', $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    /**
     * Method create detail order
     * @param Array $data Data insert
     */
    public function createOrderDetail($data){
        $this->db->insert('order_detail', $data);
    }

    public function getDataOrderHead($offset, $limit, $where){
        $sql = "SELECT order_id, 
                        customer_id, 
                        creation_date, 
                        delivery_address, 
                        concat('$ ',total) as total
                FROM test.`order` 
                $where
                ORDER BY 1 DESC
                LIMIT $limit OFFSET $offset;";
        return $this->db->query($sql)->result();
    }

    public function getTotalOrders($where){
        $sql = "SELECT COUNT(order_id) as total
                FROM test.`order`
                $where ;";
        return $this->db->query($sql)->result();
    }

    public function getProductByOrder($orderId){
        $sql = "SELECT concat( od.quantity ,' x ', p.name) AS product
                FROM order_detail od
                INNER JOIN product p ON p.product_id = od.product_id
                WHERE od.order_id = $orderId;";
        return $this->db->query($sql)->result();
    }

}
