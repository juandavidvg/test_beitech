<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model("Orders_model", "orders");
    }

    /**
     * Metodo para obtener el listado de clientes por el parametro enviado
     */
    public function getDataCustomers($search) {

        # Compruebo que la petición sea de ajax
        if ($this->input->is_ajax_request()) {

            # 
            $newSearch = str_replace('%20',' ',$search);

            # Obtengo los datos de los tipos de documentos
            $dataCustomers = $this->orders->getCustomerByName($newSearch);

            $this->output->set_status_header(200)->set_content_type('application/json')->set_output(json_encode($dataCustomers, JSON_PARTIAL_OUTPUT_ON_ERROR));
        }
    }

    /**
     * Metodo para obtener el listado de productos por el customer id
     */
    public function getDataProductsById($customer_id, $search) {
        # Compruebo que la petición sea de ajax
        if ($this->input->is_ajax_request()) {

            # 
            $newSearch = str_replace('%20',' ',$search);

            # Obtengo los datos de los tipos de documentos
            $dataProducts = $this->orders->getProductsByCustomer($customer_id, $newSearch);

            # creo array temporal para almacenar los datos
            $array_temp = array();

            # Recorro todos los elementos
            foreach($dataProducts AS $product){
                array_push($array_temp,array('product_id' => $product->product_id,
                                             'name' => strtoupper($product->name),
                                             'product_description' => strtoupper($product->product_description),
                                             'price' => $product->price));
            }

            $this->output->set_status_header(200)->set_content_type('application/json')->set_output(json_encode($array_temp, JSON_PARTIAL_OUTPUT_ON_ERROR));
        }
    }

    public function setDataOrder(){

        # Obtengo la data
        $data = json_decode(base64_decode($_POST['data']));

        # Variables del scope
        $customerId = $data->idOrderCustomer;
        $customerAddress = $data->orderAddress;
        $limitProduct = $data->productsCreate;
        $arrayProduct = [];
        $totalOrder = 0;

        # Recorro elementos para obtener las variables
        for($i = 1; $i < $limitProduct; $i++){
            if(isset($data->{'idProductCustomer'.$i})){
                # Proceso a obtener los valores de la base de datos por seguridad
                $dataDBProduct = $this->orders->getDataProduct($data->{'idProductCustomer'.$i});

                # Almaceno todo el array
                array_push($arrayProduct,['product_id' => $data->{'idProductCustomer'.$i},
                                          'quantity' => $data->{'orderQuantity'.$i},
                                          'price' => (int) $dataDBProduct[0]->price,
                                          'description' => $dataDBProduct[0]->product_description
                ]);

                # Calculo valores con la base de datos
                $price = (int) $dataDBProduct[0]->price;
                $q = (int) $data->{'orderQuantity'.$i};
                $totalOrder = $totalOrder + ($price * $q);
            }
        }

        # Creo array para insert cabecera de la orden
        $dataHead = array(
            'order_id' => '',
            'customer_id' => $customerId, 
            'creation_date' => date('Y-m-d H:i:s'), 
            'delivery_address' => $customerAddress, 
            'total' => $totalOrder
        );

        # Inserto la cabecera y return el ID
        $orderId = $this->orders->createOrder($dataHead);

        foreach($arrayProduct AS $product){

            $dataDetail = array(
                'order_detail_id' => '', 
                'order_id' => $orderId, 
                'product_id' => $product['product_id'], 
                'product_description' => $product['description'], 
                'price' => $product['price'], 
                'quantity' => $product['quantity']
            );

            # Inserto detalle en la base de datos
            $this->orders->createOrderDetail($dataDetail);
        }

        $this->output->set_status_header(200)->set_content_type('application/json')->set_output('Order Created');
    }

    public function getDataTable(){

        $where = "";
        if(isset($_GET['idCustomer'])){
            $where = "WHERE customer_id = " . $_GET['idCustomer'];
        }

        if(isset($_GET['toDate'])){
            
            if(empty($where)){
                $where = "WHERE date(creation_date) >= date('".$_GET['toDate']."')";
            }else{
                $where .= " AND date(creation_date) >= date('".$_GET['toDate']."')";
            }
        }

        if(isset($_GET['fromDate'])){
            if(empty($where)){
                $where = "WHERE date(creation_date) <= date('".$_GET['fromDate']."')";
            }else{
                $where .= " AND date(creation_date) <= date('".$_GET['fromDate']."')";
            }
        }

        # Limites a obtener
        $offset = $_GET['offset'];
        $limit = $_GET['limit'];
        
        $dataOrder = $this->orders->getDataOrderHead($offset , $limit, $where );
        $dataTotal = $this->orders->getTotalOrders($where)[0]->total;

        # Recorro los datos
        foreach($dataOrder AS $order){

            $orderId = $order->order_id;
            $dataProduct = $this->orders->getProductByOrder($orderId);

            $stringProduct = "";
            foreach($dataProduct AS $product){
                $stringProduct .= $product->product."<br>";
            }

            $stringProduct = trim($stringProduct,"<br>");

            $order->products = $stringProduct;
        }

        $arrayTable = [
            'rows' => $dataOrder,
            'total' => $dataTotal
        ];

        $this->output->set_content_type('application/json')->set_output(json_encode($arrayTable));
    }
}